package com.store.retail;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.store.retail.service.RetailService;
import com.store.retail.service.impl.RetailServiceImpl;

/**
 * Unit test for simple App.
 */
public class RetailStoreAppTest {

	private static RetailService retailSerice;
	
	@BeforeClass
	public static void initCalculator() {
		retailSerice = new RetailServiceImpl();
	}

	@Before
	public void beforeEachTest() {
		System.out.println("This is executed before each Test");
	}

	@After
	public void afterEachTest() {
		System.out.println("This is exceuted after each Test");
	}
	
	@Test
    public void discountOnBillTest() {
		retailSerice.discountOnBill(5, 100);
		System.out.println("Test case for discountOnBillTest processing...");
		assertEquals(5, retailSerice.discountOnBill(5, 100), 10);
		System.out.println("Test cases passed...");
    }
	
	
	
	@Test
    public void nearestDownValueTest() {
		retailSerice.nearestDownValue(145);
		System.out.println("Test case for nearestDownValueTest processing...");
		assertEquals(100, retailSerice.nearestDownValue(195), 100);
		System.out.println("Test cases passed...");
    }
	
	
	@Test
    public void calculateBillTest() {
		retailSerice.calculateBill(145);
		System.out.println("Test case for calculateBillTest processing...");
		assertEquals(120, retailSerice.calculateBill(195), 100);
		System.out.println("Test cases passed...");
    }
	
	
}

package com.store.retail.model;

/**
 * This is StoreModel class which is used to hold the 
 * user input data.
 *
 * @author manishk
 * @version 1.0 - 01 Dec, 2019
 */
public class StoreModel {
	
	private Integer itemType;
	private Double totalBill;
	/**
	 * @return the itemType
	 */
	public Integer getItemType() {
		return itemType;
	}
	/**
	 * @param itemType the itemType to set
	 */
	public void setItemType(Integer itemType) {
		this.itemType = itemType;
	}
	/**
	 * @return the totalBill
	 */
	public Double getTotalBill() {
		return totalBill;
	}
	/**
	 * @param totalBill the totalBill to set
	 */
	public void setTotalBill(Double totalBill) {
		this.totalBill = totalBill;
	}
	public StoreModel(Integer itemType, Double totalBill) {
		super();
		this.itemType = itemType;
		this.totalBill = totalBill;
	}
	
	public StoreModel() {
		super();
	}
	
}
package com.store.retail.service.impl;

import java.util.Scanner;

import com.store.retail.model.StoreModel;
import com.store.retail.service.RetailService;
import com.store.retail.util.ApplicationConstant;

/**
 * This is RetailServiceImpl class which contains  method like
 * nearestDownValue, discountOnBill and calculateBill to calculate the payable
 * amount accordingly various discounts like role base or transactions base.
 *
 * @author manishk
 * @version 1.0 - 06 Dec, 2019
 */
public class RetailServiceImpl implements RetailService {

	private Scanner readInput;
	private double roleBaseDiscount = 0.0, billAmountAfterDiscount = 0.0;

	/**
	 * Method that contains the logic to find the nearest down value for example 235
	 * gives 200 and 375 gives 300 and so on.
	 */
	public double nearestDownValue(double bill) {
		return bill - (bill % ApplicationConstant.HUNDRED.getValue());
	}

	/**
	 * Method that contains the logic to calculate discount on the basis of user
	 * role like employee as customer and customer affiliated by store etc.
	 */
	public double discountOnBill(double discount, double bill) {
		double roleBaseDiscount = (bill * discount) / ApplicationConstant.HUNDRED.getValue();
		System.out.println("\t Role based discount on total bill : " + roleBaseDiscount);
		return roleBaseDiscount;
	}

	/**
	 * Method that contains the logic to find the Net payable amount on the basises
	 * of role base discount and transaction base discount.
	 */
	public double calculateBill(double totalBill) {

		if (totalBill >= ApplicationConstant.HUNDRED.getValue()) {
			double transDiscount = (nearestDownValue(totalBill) * ApplicationConstant.TRANS_BASE_DISCOUNT.getValue()) / 100;
			System.out.println("\t Transaction Discount on total bill : " + transDiscount);
			totalBill = totalBill - transDiscount;
		}
		return totalBill;
	}

	/**
	 * Method that contains the logic to assign the discount on the basis of user
	 * role.
	 */
	public double discountOnRoleBase() {

		double roleBaseDiscount = 0.0;
		String userRole = null;

		System.out.println("\t Please enter the role of User :\n");
		System.out.println("\t ENTER <USER_EMP> for user as Employee.");
		System.out.println("\t ENTER  <USER_AFFILIATE> user is an Affiliate of the store.");
		System.out.println("\t ENTER  <USER_LONG_TERM> user is customer for over 2 years.\n\n");
		readInput = new Scanner(System.in);
		userRole = readInput.nextLine();
		System.out.println("\t Entered : <User Role> is :" + userRole + "\n");

		ApplicationConstant applicationConstant = ApplicationConstant.valueOf(userRole);
		
		
		switch (applicationConstant) {

		case USER_EMP:
			roleBaseDiscount = ApplicationConstant.USER_EMP_DISCOUNT.getValue();
			break;

		case USER_AFFILIATE:
			roleBaseDiscount = ApplicationConstant.USER_AFFILIATE_DISCOUNT.getValue();
			break;

		case USER_LONG_TERM:
			roleBaseDiscount = ApplicationConstant.USER_LONG_TERM_DISCOUNT.getValue();
			break;
		default:
			System.out.println("NO proper value found");
			break;

		}
		return roleBaseDiscount;
	}

	public void checkItemType(StoreModel storeModel) {

		if (ApplicationConstant.GROCERIES_ITEM.getValue() == storeModel.getItemType())
			roleBaseDiscount = 0.0;
		else
			roleBaseDiscount = discountOnRoleBase();

		if (roleBaseDiscount > 0)
			billAmountAfterDiscount = storeModel.getTotalBill()
					- discountOnBill(roleBaseDiscount, storeModel.getTotalBill());

		else {
			billAmountAfterDiscount = storeModel.getTotalBill();
			System.out.println("\t Role based discount on total bill : " + roleBaseDiscount);
		}

		System.out.println("\n\t Net Payable Amount : " + calculateBill(billAmountAfterDiscount));

	}

}
package com.store.retail.service;

import com.store.retail.model.StoreModel;

/**
 * This is RetailService Interface contains method like
 *  checkItem type, discountOnRole,nearestDownValue,calculateBill and discountOnBill.
 *
 * @author manishk
 * @version 1.0 - 06 Dec, 2019
 */
public interface RetailService {

	void checkItemType(StoreModel storeModel);
	
	double discountOnRoleBase();

	double nearestDownValue(double bill);

	double calculateBill(double totalBill);

	double discountOnBill(double discount, double bill);

}

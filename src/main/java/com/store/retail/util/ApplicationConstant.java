package com.store.retail.util;

public enum ApplicationConstant {
	
	
	USER_EMP(1),
	USER_AFFILIATE(2),
	USER_LONG_TERM(3),
	GROCERIES_ITEM(1),
	TRANS_BASE_DISCOUNT(5),
	USER_EMP_DISCOUNT (30),
	USER_AFFILIATE_DISCOUNT(10),
	USER_LONG_TERM_DISCOUNT(5),
	HUNDRED (100);

	private int value;
	
	public int getValue(){
		return value;
	}
	
	ApplicationConstant(int value){
		this.value = value;
	}
}

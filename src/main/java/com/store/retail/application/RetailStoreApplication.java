package com.store.retail.application;

import java.util.Scanner;

import com.store.retail.model.StoreModel;
import com.store.retail.service.RetailService;
import com.store.retail.service.impl.RetailServiceImpl;

/**
 * This is RetailStoreApplication class which is the entry point of application. 
 * It takes user input data like grocery item and bill amount.
 *
 * @author manishk
 * @version 1.0 - 01 Dec, 2019
 */

public class RetailStoreApplication {

	private Scanner readInput;

	public static void main(String[] args) {
		RetailService retailService = new RetailServiceImpl();
		retailService.checkItemType(new RetailStoreApplication().userInput());
	}

	
	/**
	 * Method that contains the logic to get input from user and 
	 * set into model class.
	 */
	public StoreModel userInput() {

		double totalBill = 0.0;
		int itemType = 0;

		readInput = new Scanner(System.in);

		System.out.println("\t Please enter the Item Type\n");
		System.out.println("\t Press <1> for Groceries Item.");
		System.out.println("\t Press <2> for Non-Groceries Item.");
		itemType = readInput.nextInt();

		System.out.println("\t Enter total bill amount.");
		totalBill = readInput.nextInt();
		System.out.println("\t Entered : <Item type> is  :" + itemType + "\n");

		return new StoreModel(itemType, totalBill);
	}

}

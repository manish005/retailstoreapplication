package profesorp.oauth2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TagMasterMicroServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(TagMasterMicroServiceApplication.class, args);
	}

	
	/*
	 * @Autowired UserDetailsService usreDetailsService;
	 * 
	 * 
	 * @Autowired public void authenticationManager(AuthenticationManagerBuilder
	 * authenticationManagerBuilder) throws Exception {
	 * authenticationManagerBuilder.userDetailsService(usreDetailsService);
	 * 
	 * }
	 */
	
}

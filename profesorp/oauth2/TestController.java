package profesorp.oauth2;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
	
	  @GetMapping("/public")
	  public String publico() {
	    return "Pagina Publica";
	  }
	  @PostMapping("/private")
	  public String privada() {
	    return "Pagina Privada";
	  }
	  @PostMapping("/admin")
	  public String admin() {
	    return "Pagina Administrador";
	  }

}

package profesorp.oauth2;



import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.web.bind.annotation.RestController;


@EnableResourceServer
@RestController
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter
{
	
	  @Override
		public void configure(HttpSecurity http) throws Exception {
			http
			.authorizeRequests().antMatchers("/oauth/token", "/oauth/authorize**", "/public").permitAll();
//			 .anyRequest().authenticated();
			http.requestMatchers().antMatchers("/admin","/private")
			.and().authorizeRequests()
			.antMatchers("/admin","/private").access("hasRole('ADMIN')")
			.and()
			.requestMatchers().antMatchers("/private")
			.and()
			.authorizeRequests()
			.antMatchers("/private").access("hasRole('USER')");
		}   

}

